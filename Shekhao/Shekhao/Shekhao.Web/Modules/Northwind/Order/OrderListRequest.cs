﻿using Serenity.Services;

namespace Shekhao.Northwind
{
    public class OrderListRequest : ListRequest
    {
        public int? ProductID { get; set; }
    }
}
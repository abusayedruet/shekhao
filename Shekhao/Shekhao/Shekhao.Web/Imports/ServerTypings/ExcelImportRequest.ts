﻿namespace Shekhao {
    export interface ExcelImportRequest extends Serenity.ServiceRequest {
        FileName?: string;
    }
}


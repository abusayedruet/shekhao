﻿namespace Shekhao.Northwind {
    export enum Gender {
        Male = 1,
        Female = 2
    }
    Serenity.Decorators.registerEnumType(Gender, 'Shekhao.Northwind.Gender', 'Shekhao.Northwind.Entities.Gender');
}

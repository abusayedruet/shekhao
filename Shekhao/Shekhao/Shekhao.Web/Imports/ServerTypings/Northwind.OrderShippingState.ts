﻿namespace Shekhao.Northwind {
    export enum OrderShippingState {
        NotShipped = 0,
        Shipped = 1
    }
    Serenity.Decorators.registerEnumType(OrderShippingState, 'Shekhao.Northwind.OrderShippingState', 'Northwind.OrderShippingState');
}
